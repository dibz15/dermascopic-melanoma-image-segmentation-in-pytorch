# Dermascopic Melanoma Image Segmentation in PyTorch

- [Dermascopic Melanoma Image Segmentation in PyTorch](#dermascopic-melanoma-image-segmentation-in-pytorch)
  - [About](#about)
    - [Trying it out](#trying-it-out)
    - [Examples](#examples)
  - [Training the Model](#training-the-model)
    - [Example](#example)
  - [Scripts](#scripts)
    - [main.py](#mainpy)
      - [main.py Arguments](#mainpy-arguments)
      - [Resuming training](#resuming-training)
    - [runModel.py](#runmodelpy)
      - [runModel.py Arguments](#runmodelpy-arguments)
    - [testAccuracy.py](#testaccuracypy)
      - [testAccuracy.py Arguments](#testaccuracypy-arguments)
      - [testAccuracy.py Example](#testaccuracypy-example)
    - [constructDataset.py](#constructdatasetpy)
      - [constructDataset.py Arguments](#constructdatasetpy-arguments)
  - [Appendix](#appendix)
    - [A1. ISIC Archive Downloader](#a1-isic-archive-downloader)
    - [A2. Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation)
    - [A3. Intersection Over Union (IoU)](#a3-intersection-over-union-iou)

## About

<table>
<tr>
<td> <img src="./sample_media/ISIC_0011490.jpeg" alt="unmasked" style="width: 250px;"/> </td>
<td> <img src="./sample_media/ISIC_0011490_mask.jpg" alt="mask" style="width: 250px;"/> </td>
</tr>
<tr>
<td> <img src="./sample_media/ISIC_0011490_masked.jpg" alt="masked" style="width: 250px;"/> </td>
<td> <img src="./sample_media/ISIC_0011490_cropped.jpg" alt="cropped" style="width: 250px;"/> </td>
</tr>
</table>

This repository is focused on the creation of a deep learning model that can detect and mask dermascopic images of benign/malignant mole samples. In the end, I was able to create a model that can automatically find and mask moles from their original noisy images, and this may be useful in the building of image/mask datasets for melanoma classification problems. 

Note that this project is not for _classification_, just _detection_ and/or _masking_. It can be used to create images that could be passed to a classifier, or stored for later processing. 

The mask model utilizes the MaskRCNN model structure, and you can read more about its implementation in PyTorch [here](https://pytorch.org/vision/master/generated/torchvision.models.detection.maskrcnn_resnet50_fpn.html?highlight=maskrcnn#torchvision.models.detection.maskrcnn_resnet50_fpn). 

My goal was to make a model that could be run on lower-end devices, thus the backbone uses the MobileNetV2 architecture rather than a larger ResNet architecture (the default in PyTorch) that may perform better. At the time that I was working on this project, MobileNetV2 was state-of-the-art, it may be worth trying now other mobile-focused architectures that have been developed since then like MobileNetV3-Large or the EfficientNet family of models. 

### Trying it out

I have in this repository a model that I trained on an RTX2060 for 100 epochs (model reached convergence) stored in trained_model.tar.gz. To try this trained model on the image, simply unzip the trained model and run the following script:

    python runModel.py --input_file <image-path> --model_file trained_model.pth

This will evaluate the input image file and display the masked version of the image. 

### Examples

Here are some examples using different command line options to change the output for an image. 

If we would simply like to output a segmentation mask for the image at `./sample_media/ISIC_0011471.jpeg` and output it at `./sample_media/ISIC_0011471_mask.jpg`, we could use this command:

    python runModel.py --input_file ./sample_media/ISIC_0011471.jpeg --model_file trained_model.pth --onlymask --output_file ./sample_media/ISIC_0011471_mask.jpg

Then our output image looks like this (left image). Compare it to the ground-truth mask on the right. You can see that our mask isn't as precise, but it does ignores the hair in the image while the ground-truth mask does not.

<table><tr>
<td> <img src="./sample_media/ISIC_0011471_mask.jpg" alt="Mask" style="width: 250px;"/> </td>
<td> <img src="./sample_media/ISIC_0011471_novice.png" alt="Mask ground truth" style="width: 250px;"/> </td>
</tr></table>

The important part above is the `--onlymask` flag which instructs the script to just write out the segmentation mask. If we instead wish to output the _masked_ output image, we could omit the `--onlymask` flag. Then the output is like so:  

![Masked ISIC image](./sample_media/ISIC_0011471_masked.jpeg)

Or, if you would simply like to get an output image that is cropped to the mole in the image, then we can do this:

    python runModel.py --input_file ./sample_media/ISIC_0011471.jpeg --model_file trained_model.pth  --nomask --crop --output_file ./sample_media/ISIC_0011471_cropped.jpg

Here the `--nomask` flag tells it to not mask the output image, and `--crop` instructs it to crop the output image down to the masked area, scaled up by 10%.

![Masked ISIC image](./sample_media/ISIC_0011471_cropped.jpg)


## Training the Model

To train the model yourself, you need to setup the ISIC dataset in the way the script expects, and then run `main.py` with the proper arguments. To prepare the dataset for training, see [Appendix A2: Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation) and then come back here.

Once you have the dataset prepared in a structure like so:

    dataset_root
        /ISICTest
        /ISICTrain
        /ISICVal

or a similar configuration (the key is just to have separate training and validation sets) then you can begin training with the main script. 

### Example

Assuming you have the dataset at ./dataset, your training command may look like this:

    python main.py --cuda --dataset_root ./dataset

This will start training the MaskRCNN model to detect and mask moles in dermascopic images. There are many different command-line arguments that can be set such as batch size, learning rate, and so on. You can read more about those and how to use this script more under [Scripts - main.py](#mainpy).

Note: this script utilizes the `SummaryWriter` class from `torch.utils.tensorboard` for integration with Tensorboard. While it runs, it will save data into the `./runs` directory. If you wish to visualize it, and have tensorboard installed, just run `tensorboard --logdir runs` in a separate terminal while `main.py` is running.

----

## Scripts

### main.py

As discussed some in the [training example](#example) this script is used for training a new model. Here's the output of `python main.py -h`.

    usage: main.py [-h] [--image_scale IMAGE_SCALE] [--batchSize BATCHSIZE]
               [--valBatchSize VALBATCHSIZE] [--epochs EPOCHS] [--lr LR]
               [--cuda] [--threads THREADS] [--seed SEED] [--freeze_backbone]
               [--checkpoint_dir CHECKPOINT_DIR]
               [--model_save_dir MODEL_SAVE_DIR] [--dataset_root DATASET_ROOT]
               [--train_num_images TRAIN_NUM_IMAGES]
               [--val_num_images VAL_NUM_IMAGES]
               [--load_checkpoint LOAD_CHECKPOINT]

#### main.py Arguments

- `--image_scale`: Default = 1.0 : A float scale factor by which to scale the training/eval images. Default is 1.0. Training is faster if this is smaller, such as 0.5.
- `--batchSize`: Default = 5. Training batch size. 
- `--valBatchSize`: Default = 10. Validation batch size.
- `--cuda`: Turn on CUDA.
- `--threads`: Default = 4. Number of threads with which to load training data.
- `--epochs`: Default = 10. Number of epochs to train.
- `--lr`: Default = 0.001. Learning rate.
- `--seed`: Default = 345. Random seed.
- `--freeze_backbone`: Freeze the weights of the backbone classifier model (just train masking weights). Speeds up training, may decrease training accuracy.
- `--checkpoint_dir`: Default = './checkpoints'. Directory to store checkpoint data.
- `--model_save_dir`: Default = './saved_models'. Directory to write out final trained models.
- `--dataset_root`: Default = './dataset'. Directory root for training/validation data. This folder should contain ISICTrain and ISICVal.
- `--train_num_images`: Default = all. Specify number of images on which to train from the set. Default is the entire dataset. The images are randomly sampled from the dataset.
- `--val_num_images`: Default = all. Specify the number of images to test for validation. Default is the entire validation set. The images are randomly sampled.
- `--load_checkpoint`: Checkpoint *.pth file to load and resume training. Checkpoints have more information in them than the final model output.

#### Resuming training

If training fails for some reason, or for some reason you would like to resume training from a previously written checkpoint, the script can resume quickly given the checkpoint file. 
Example: 

    python main.py --cude --load_checkpoint ./checkpoints/run###/model_epoch_##.pth

The ## can be replaced with your specific checkpoint path data. Doing this will resume training where it left off for the given checkpoint.

### runModel.py

`runModel.py` can be used to run the final trained model against any images/videos you would like and has different modes of output. Some examples can be seen at the top of this readme in [examples](#examples).

#### runModel.py Arguments

usage: runModel.py [-h] [--cuda] --model_file MODEL_FILE
                   [--score_threshold SCORE_THRESHOLD]
                   [--iou_threshold IOU_THRESHOLD]
                   [--model_input_size MODEL_INPUT_SIZE]
                   [--input_file INPUT_FILE] [--output_file OUTPUT_FILE]
                   [--in_type IN_TYPE] [--noshow] [--crop] [--nomask]
                   [--onlymask]

- `--model_file`: trained model file to run.
- `--score_threshold`: Default = 0.5. This is the model's confidence score threshold for prediction. Any predictions with confidence less than this will be ignored.
- `--iou_threshold`: Default = 0.4. [IoU score](#a3-intersection-over-union-iou) threshold for bounding box prediction.
- `--model_input_size`: Default = 512. The input image is bound to this as the maximum width or height dimension. If it is above this on either axis, it is scaled down before inference.
- `--input_file`: media input file on which to run the model. If it is a video, you must specify `--in_type v`.
- `--in_type`: Media input type. `v` for video, `img` for images, `w` for webcam.
- `--output_file`: Supply this if you would like to write out the modified media to disk.
- `--noshow`: Don't show an cv2 frames with the modified media. Useful if you just want to write the result to disk and not display it.
- `--crop`: Crop the output media to the discovered mole. Can be masked or unmasked. If you wish it to be unmasked, specify `--nomask`.
- `--nomask`: Don't mask the output media.
- `--onlymask`: Only output the mask. This can be paired with `--crop`.

### testAccuracy.py

Use this script to test the overall stats of a trained model (or checkpoint). When run and compared using a test dataset, this may give another metric for comparing models.

#### testAccuracy.py Arguments

    usage: testAccuracy.py [-h] [--cuda] [--checkpoint_folder CHECKPOINT_FOLDER]
                          [--model_file MODEL_FILE]
                          [--score_threshold SCORE_THRESHOLD]
                          [--dataset_root DATASET_ROOT] [--imageScale IMAGESCALE]
                          [--max_input_size MAX_INPUT_SIZE]

- `--checkpoint_folder`: Providing this will have the script will have the script test, calculate, and plot the average F1 score for each model in the given checkpoint directory.
- `--model_file`: Providing this will run the test on just this one model file.
- `--score_threshold`: Default = 0.5. This is the model's confidence score threshold for prediction. Any predictions with confidence less than this will be ignored.
- `--dataset_root`: Default = './dataset'. Root for the prepared ISIC dataset files. Tests against <root>/ISICTest
- `--imageScale`: Default = 1.0. Float factor for how much to scale down (or up) the input images.
- `--max_input_size`: Default = 1024. Int in pixels for the max image dimension for the input images. E.g. if 1024, then then images will be scaled such that their largest dimension is 1024px.

#### testAccuracy.py Example

  python testAccuracy.py --cuda --model_file ./trained_model.pth --score_threshold 0.4 --dataset_root ./SegDataset/ --imageScale 0.5
  

Output:

    Avg IoU: 0.7507354578090958
    Avg F1: 0.8443209463342578
    Recall: 0.9055727554179567
    Precision: 0.9653465346534653
    Accuracy: 0.8770614692653673

Note that in this case, a true positive is considered if the f1 overlap is > 0.5. If you want to change this for yourself, it can be set at line 131.

### constructDataset.py

This script organizes the ISIC data in a way that can be used with the other scripts in this project for training and testing a MaskRCNN model. You can read more about the dataset structure under [Appendix - A2. Preparing the ISIC Dataset for Training & Evaluation](#a2-preparing-the-isic-dataset-for-training--evaluation).

#### constructDataset.py Arguments

    usage: constructDataset.py [-h] [--root ROOT] [--percentVal PERCENTVAL]
                              [--percentTest PERCENTTEST] [--output OUTPUT]

- `--root`: Default = './ISIC'. Root of the downloaded ISIC archive dataset.
- `--percentVal`: Default = 0.2. Percent of the dataset to set as validation data.
- `--percentTest`: Default = 0.05. Percent of the dataset to set as test data.
- `--output`: Default = './dataset'. Root of the output dataset with train, val, and test data.

-----

## Appendix

### A1. ISIC Archive Downloader

For this project, I utilized the scripts at [https://github.com/GalAvineri/ISIC-Archive-Downloader](https://github.com/GalAvineri/ISIC-Archive-Downloade). If you would like to use the same script to download the dataset yourself, the descriptions, images, and segmentation masks can all be downloaded with this line:

    python download_archive.py -s

And note that it by default downloads everything into the `./dataset` folder wherever you launch the script. I've found that the total downloaded dataset is > _100GiB_, so make sure you have enough space!

### A2. Preparing the ISIC Dataset for Training & Evaluation

The scripts `main.py` and `testAccuracy.py` in this project expect the ISIC dataset to be split into train, val, and test datasets so that training and inference can take place. These datasets are expected to be in a directory structure like this:

    dataset_root
      \ISICTrain
        \Images
        \Segmentations
        \dataset.txt
      \ISICVal
        ...
      \ISICTest
        ...

Under each ISIC* subdirectory, there are the images, their corresponding segmentations, and a `dataset.txt` file which describes the bounding box for each image (this is so that the MaskRCNN model has the data it expects for training) and maps each image to its segmentation image.

To build this dataset, you can simply run the `constructDataset.py` script with the proper arguments and it will organize the dataset as specified above.

Example:

    python constructDataset.py --root ./ISIC --percentVal 20 --percentTest 10 --output ./dataset

It can also be run without arguments, using the default options. You can read more about the options for this command above under [Scripts - constructDataset.py](#constructdatasetpy).

### A3. Intersection Over Union (IoU)

Intersection over Union bounding box comparison considers a bounding box overlapping another bounding box if their 'IoU' value is greather than some threshold. For IoU threshold 0.4 this means that if the predicted box is overlapping the ground truth by at least 40% then it is considered a true positive. If a box overlaps less than 40% it is a false positive, and if there is a ground truth box without an overlapping predicted box then it is a false negative. Everything else would be a true negative.

Read more [here](https://towardsdatascience.com/intersection-over-union-iou-calculation-for-evaluating-an-image-segmentation-model-8b22e2e84686).

The threshold for this IoU calculation is set with the commandline argument `--iou_threshold`. See [Commandline Arguments](#commandline-arguments). 
A useful comparison measurement for object detection models is mAP (mean average precision) of a model. Often mAP is charted vs. IoU.

Read more about mAP [here](https://towardsdatascience.com/implementation-of-mean-average-precision-map-with-non-maximum-suppression-f9311eb92522).
