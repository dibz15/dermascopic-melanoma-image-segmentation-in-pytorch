import os
import numpy as np
import torch
from PIL import Image
import cv2
from torchvision import transforms
import matplotlib.pyplot as plt

mean_sums = [0.485, 0.456, 0.406]
std_sums = [0.229, 0.224, 0.225]

def getMaskTransforms():
    return transforms.Compose([
        transforms.ToTensor()
        # transforms.Normalize(mean_sums, std_sums)
    ])

def getBlackList(blacklistFile):
    if blacklistFile is None:
        return None

    fileLines = []
    with open(blacklistFile) as bF:
        fileLines = [line.rstrip() for line in bF]

    return set(fileLines)

#Root should end in Images, Segmentations, and dataset.txt
def loadDataset(root, datasetFile, max_w=1022, max_h=767, blackList=None):
    basePath = root

    imagePaths = []
    segmentPaths = []
    boundingBoxes = []

    fileLines = []
    with open(os.path.join(basePath, datasetFile)) as f:
        for line in f.readlines():
            line = line.rstrip()
            fileLines.append(line)

    for line in fileLines:
        splitLine = line.split('\t')
        imagePath, segmentPath = splitLine[0], splitLine[1]

        if (blackList is not None and imagePath not in blackList) \
            or (blackList is None):

            x1 = int(splitLine[2])
            y1 = int(splitLine[3])
            x2 = int(splitLine[4])
            y2 = int(splitLine[5])

            w = int(x2 - x1)
            h = int(y2 - y1)

            if (max(w, 0) <= 10 or max(h, 0) <= 10):
                continue

            heightLimit = int(max_h * 0.95)
            widthLimit = int(max_w * 0.95)

            if ((h >= heightLimit) or (w >= widthLimit)):
                continue

            imagePaths.append(str(os.path.join(basePath, imagePath)))
            segmentPaths.append(str(os.path.join(basePath, segmentPath)))
            boundingBoxes.append([x1, y1, x2, y2])

    return imagePaths, segmentPaths, boundingBoxes

def heatmap2d(arr: np.ndarray, title: str):
    plt.imshow(arr, cmap='viridis')
    plt.colorbar()
    plt.title(title)
    plt.show()

def calculateBoxFeatures(boundingBoxes, imageScale):
    w_h = np.array([[(x2-x1) * imageScale, (y2-y1)*imageScale] for x1, y1, x2, y2 in boundingBoxes])
    
    mins = np.amin(w_h, axis=0)
    maxes = np.amax(w_h, axis=0)

    w_range = int(maxes[0]) - 0 + 1
    h_range = int(maxes[1]) - 0 + 1
    buckets = np.zeros((h_range, w_range), dtype=np.float32)
    for pair in w_h:
        buckets[int(pair[1])][int(pair[0])] += 1

    heatmap2d(np.log10(buckets), 'W x H distribution')

    areas = np.array([(x2 - x1) * (y2 - y1) * imageScale * imageScale for x1, y1, x2, y2 in boundingBoxes])

    plt.figure()
    plt.hist(np.sqrt(areas), int(1000), cumulative=True)
    plt.title('Approx. Distribution of square sizes.')
    plt.show()

    aspect_ratios = np.array([(x2 - x1) / (y2 - y1) for x1, y1, x2, y2 in boundingBoxes])

    plt.figure()
    plt.hist(aspect_ratios, int(1000), cumulative = True)
    plt.title('Approx. Distribution of aspect ratios')
    plt.show()

def getModifiedImgDimensions(origW, origH, imgScale, minSize=None, maxSize=None):

    scaledImgW, scaledImgH = origW * imgScale, origH * imgScale

    if maxSize is not None and (scaledImgH > maxSize or scaledImgW > maxSize):
        proportion = maxSize / max(scaledImgW, scaledImgH) 
    elif minSize is not None and (scaledImgH < minSize or scaledImgW < minSize):
        proportion = minSize / min(scaledImgH, scaledImgW)
    else:
        proportion = 1
        
    scaledImgW *= proportion
    scaledImgH *= proportion

    return round(scaledImgW), round(scaledImgH)

class ISICSegmentDataset(torch.utils.data.Dataset):
    def __init__(self, root, transforms=None, imageScale=1.0, blackListFile=None, minSize=None, maxSize=None, grayscale=False):
        self.root = root
        self.transforms = transforms

        self.blackList = getBlackList(blackListFile)

        self.imagePaths, self.maskPaths, self.boundingBoxes = loadDataset(root, 'dataset.txt', blackList=self.blackList)

        self.imageScale = imageScale

        self.minSize = minSize
        self.maxSize = maxSize
        self.grayscale = grayscale
        #Just to figure out the distribution of box sizes and aspect ratios...
        #calculateBoxFeatures(self.boundingBoxes, imageScale)

    def __getitem__(self, idx):
        try:
            imagePath = self.imagePaths[idx]
            maskPath = self.maskPaths[idx]
            bbox = self.boundingBoxes[idx]
        except Exception as error:
            print('Exception at', idx, error)
            return None, None

        #Load image and mask
        img = Image.open(imagePath).convert("RGB")
        mask = Image.open(maskPath).convert("RGB")
        originalImgWidth, originalImgHeight = img.size
        #print(originalImgWidth, originalImgHeight)

        if self.transforms is not None:
            scaledWidth, scaledHeight = getModifiedImgDimensions(originalImgWidth, originalImgHeight, self.imageScale, self.minSize, self.maxSize)
            img = cv2.resize(np.array(img), (int(scaledWidth), int(scaledHeight)))
            if self.grayscale:
                img = cv2.cvtColor(cv2.cvtColor(img, cv2.COLOR_RGB2GRAY), cv2.COLOR_GRAY2RGB)
            img = Image.fromarray(img)
            img = self.transforms(img)

            mask = cv2.resize(np.array(mask), (int(scaledWidth), int(scaledHeight)))
            _, mask = cv2.threshold(mask, 50, 255, cv2.THRESH_BINARY)
            mask = Image.fromarray(mask) #Convert to PIL 영상
            mask = getMaskTransforms()(mask) #Convert to Tensor
            mask = np.array(mask[0, :, :], dtype=np.uint8)  #Discard other color channels

        if torch.isnan(img).any():
            print('Image {} has nan value after transform.'.format(imagePath))

        if np.isnan(mask).any():
            print('Mask {} has nan value after transform.'.format(maskPath))

        resizedImgWidth, resizedImgHeight = img.size()[2], img.size()[1]

        faceBox = bbox
        #print('Original face bbox:', faceBox)
        xmin = faceBox[0] / originalImgWidth * resizedImgWidth
        ymin = faceBox[1] / originalImgHeight * resizedImgHeight
        xmax = xmin + (faceBox[2] / originalImgWidth * resizedImgWidth)
        ymax = ymin + (faceBox[3] / originalImgHeight * resizedImgHeight)
        t = [xmin, ymin, xmax, ymax]

        #For segmentation there's only one class label, and only one in each mask
        #MaskRCNN expects labels as Int64
        labels = torch.ones((1,), dtype=torch.int32)
        boxes = torch.as_tensor([t,], dtype=torch.float32)
        masks = torch.as_tensor(np.array([mask]), dtype=torch.uint8)
        #print(masks.shape)
        #FasterRCNN expects a target dict
        target = {}
        target['masks'] = masks
        target['boxes'] = boxes
        target['labels'] = labels
        target['imagePath'] = imagePath

        return img, target

    def __len__(self):
        return len(self.imagePaths)