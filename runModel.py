import torch

from PIL import Image
import math, time, copy, os

from utils import *
from torchvision.models.detection.faster_rcnn import FasterRCNN

import cv2
import numpy as np

import argparse
import matplotlib.pyplot as plt

parser = argparse.ArgumentParser(description='FasterRCNN with MobileNetV2 on WIDER Face Dataset.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--model_file', type=str, required=True, help="pre-trained model file")
parser.add_argument('--score_threshold', type=float, default=0.5, help="Threshold for box scoring.")
parser.add_argument('--iou_threshold', type=float, default=0.4, help="Threshold for box scoring.")

#OpenCV arguments
parser.add_argument('--model_input_size', type=float, default=512, help="Amount by which to downscale dataset images.")
parser.add_argument('--input_file', type=str, help="image to run model on")
parser.add_argument('--output_file', type=str, help='Output file to write final video or image if required')
parser.add_argument('--in_type', type=str, default='w', help='Input type. \'v\' for video, \'img\' for image, \'w\' for webcam.')
parser.add_argument('--noshow', action='store_true', help='Don\'t show output preview.')
parser.add_argument('--crop', action='store_true', help='Don\'t crop output image to the mask area.')
parser.add_argument('--nomask', action='store_true', help='Save found mole unmasked, using bounding box.')
parser.add_argument('--onlymask', action='store_true', help='Just show and save mask segmentation.')


#Parse Arguments
opt = parser.parse_args()

if not opt.input_file and not opt.in_type == 'w':
    print('If --in_type is not \'w\' then --input_file is required.')
    quit()
elif opt.input_file and opt.in_type == 'w':
    opt.in_type = 'img'

#Load and prepare model ====================
device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')
model = torch.load(opt.model_file, map_location=device)

if type(model) == dict:
    if 'full_model' in model.keys() and model['full_model'] is not None:
        model = model['full_model']
    elif 'model_state' in model.keys() and model['model_state'] is not None:
        if 'backbone_state' in model.keys():
            model = getModifiedMaskRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
        else:
            model = getModifiedMaskRCNN(model_state=model['model_state'])

model = model.to(device)
model.eval()
#===========================================

#===========================================
#If source is image, open and predict
if opt.in_type == 'img' and opt.input_file:
    image = cv2.imread(opt.input_file)

    if image is None:
        print("Error opening file", opt.input_file, "as image.")
        exit(1)

    (oh, ow) = image.shape[:2]

    proportion = opt.model_input_size / max(oh, ow) 

    resizedImage = cv2.resize(image, (int(ow * proportion), int(oh * proportion)))

    modelInput = [getTestTransforms()(resizedImage).to(device)]
    pred = model(modelInput)
    sortedDict = sortPredictionsByClass(pred, 1)

    boxes = sortedDict[1]
    boxPicks = nonMaximumSuppression_pick(boxes, confThresh=opt.score_threshold, iouThresh=opt.iou_threshold)
    boxes = boxes[boxPicks]

    cleanedMasks = cleanupMasks(pred)
    maskPicks = cleanedMasks[0][boxPicks] 

    print(f'Moles found: {len(maskPicks)}')

    foundBoxes = []
    for i in range(len(maskPicks)):
        mask = maskPicks[i] * 255

        mask[np.where(mask >= 127)] = 255
        mask[np.where(mask < 127)] = 0
        mask = np.array(mask, dtype=np.uint8)

        contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        bounding_boxes = [cv2.boundingRect(contour) for contour in contours]

        # Change bounding box format to x1,y1,x2,y2 and also sort largest -> smallest
        bounding_boxes = [[x, y, x+w, y+h] for x,y,w,h in sorted(bounding_boxes, key=lambda box: box[2]*box[3], reverse=True)]
        scaleBoxes(bounding_boxes, scale=1.1)
        foundBoxes += [[x1,y1,x2,y2] for x1,y1,x2,y2 in bounding_boxes]

        if opt.onlymask:
            if not opt.noshow:
                cv2.imshow(f'Masked {i}', mask)
            maskedImg = mask
            maskedImg = cv2.resize(maskedImg, (ow, oh), interpolation=cv2.INTER_NEAREST)
        else:
            maskRGB = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
            # cvPutBoxes(maskRGB, bounding_boxes)
            # cv2.drawContours(maskRGB, contours, -1, (0, 255, 0), 3)
            # if opt.out_scaled:
            maskRGB = cv2.resize(maskRGB, (ow, oh), interpolation=cv2.INTER_NEAREST)
            scaleBoxes(bounding_boxes, 1 / proportion, center=(0, 0))

            img2Mask = image# if opt.out_scaled else resizedImage
            maskedImg = cv2.bitwise_and(img2Mask, maskRGB) if not opt.nomask else img2Mask

            if opt.crop and len(bounding_boxes):
                x1, y1, x2, y2 = bounding_boxes[0] # Just crop with largest box
                x1 = max(0, min(x1, ow))
                y1 = max(0, min(y1, oh))
                x2 = max(0, min(x2, ow))
                y2 = max(0, min(y2, oh))
                if x2 - x1 <=2 or y2 - y1 <= 2:
                    continue
                maskedImg = maskedImg[int(y1):int(y2), int(x1):int(x2)]

            #cv2.imshow('Mask', mask)
            if not opt.noshow:
                cv2.imshow(f'Masked Image {i}', maskedImg)

        if opt.output_file:
            if opt.output_file.endswith('jpeg') or opt.output_file.endswith('jpg'):
                cv2.imwrite(opt.output_file, maskedImg, [int(cv2.IMWRITE_JPEG_QUALITY), 100])
            else:
                cv2.imwrite(opt.output_file, maskedImg)

    if not opt.noshow:
        copiedImg = resizedImage.copy()
        cvPutBoxes(copiedImg, foundBoxes)
        cv2.imshow('Image', copiedImg) 
        cv2.waitKey()


elif not opt.input_file or opt.in_type == 'v':
    if (opt.input_file):
        cap = cv2.VideoCapture(opt.input_file)
    else:
        cap = cv2.VideoCapture(0)

    if not cap.isOpened():
        print("Error opening video file", opt.input_file)
        exit(1)

    fps = int(cap.get(cv2.CAP_PROP_FPS))
    iw = int(cap.get(3))
    ih = int(cap.get(4))
    proportion = opt.model_input_size / max(ih, iw) 

    ow, oh =  int(iw * proportion), int(ih * proportion)

    vw = None
    if opt.output_file:
        vw = cv2.VideoWriter(opt.output_file, cv2.VideoWriter_fourcc('M', 'J', 'P', 'G'), fps, (ow, oh))

    while (cap.isOpened()):
        ret, frame = cap.read()
        if ret:
            resizedFrame = cv2.resize(frame, (ow, oh))

            modelInput = [getTestTransforms()(resizedFrame).to(device)]
            pre_time = time.time()
            pred = model(modelInput)
            post_time = time.time()
            time_delta = post_time - pre_time
            # print(1 / time_delta)

            sortedDict = sortPredictionsByClass(pred, 1)

            boxes = sortedDict[1]
            boxPicks = nonMaximumSuppression_pick(boxes, confThresh=opt.score_threshold, iouThresh=opt.iou_threshold)
            boxes = boxes[boxPicks]

            img = resizedFrame
            # cvPutBoxes(img, boxes)

            cleanedMasks = cleanupMasks(pred)
            maskPicks = cleanedMasks[0][boxPicks]

            foundBoxes = []
            for i in range(len(maskPicks)):
                mask = maskPicks[i] * 255

                mask[np.where(mask >= 127)] = 255
                mask[np.where(mask < 127)] = 0
                mask = np.array(mask, dtype=np.uint8)

                contours, hierarchy = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
                bounding_boxes = [cv2.boundingRect(contour) for contour in contours]

                if opt.crop and not len(bounding_boxes):
                    continue

                bounding_boxes = [[x, y, x+w, y+h] for x,y,w,h in bounding_boxes]
                scaleBoxes(bounding_boxes, scale=1.4)
                foundBoxes += [[x1,y1,x2,y2] for x1,y1,x2,y2 in bounding_boxes]

                maskRGB = cv2.cvtColor(mask, cv2.COLOR_GRAY2RGB)
                # cvPutBoxes(maskRGB, bounding_boxes)
                # cv2.drawContours(maskRGB, contours, -1, (0, 255, 0), 3)
                maskRGB = cv2.resize(maskRGB, (iw, ih))
                scaleBoxes(bounding_boxes, 1 / proportion, center=(0, 0))

                img2Mask = frame 
                maskedImg = cv2.bitwise_and(img2Mask, maskRGB) if not opt.nomask else img2Mask

                if opt.crop and len(bounding_boxes):
                    x1, y1, x2, y2 = bounding_boxes[0]
                    if x2 - x1 <= 2 or y2 - y1 <= 2:
                        continue
                    maskedImg = maskedImg[int(y1):int(y2), int(x1):int(x2)]

                #cv2.imshow('Mask', mask)
                if not opt.noshow:
                    cv2.imshow('Masked Image', maskedImg)
               
            cvPutBoxes(img, foundBoxes)
            cv2.imshow('Image', img)
            if vw is not None:
                vw.write(img)

            #Press Q to exit video
            if cv2.waitKey(5) & 0xFF == ord('q'):
                break

        else:
            break

    cap.release()
    
    if vw is not None:
        vw.release()

    cv2.destroyAllWindows()