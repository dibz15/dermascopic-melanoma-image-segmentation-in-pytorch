

import os
import argparse

import numpy as np

from shutil import copy2
from shutil import rmtree

import cv2

parser = argparse.ArgumentParser(description='Construct a dataset usable for segmentation from ISIC')
parser.add_argument('--root', type=str, default='./ISIC', help='Root directory of ISIC dataset.')
parser.add_argument('--percentVal', type=float, default=0.2, help="Proportion of total dataset that will become validation")
parser.add_argument('--percentTest', type=float, default=0.05, help='Proportion of total dataset that will become test data')
parser.add_argument('--output', type=str, default='./dataset', help='Output directory path to create new segmentation dataset')
opt = parser.parse_args()

if opt.percentVal + opt.percentTest >= 1.0:
    print('Validation and Test percentage shouldn\'t exceed the whole.')
    exit(1)

if opt.percentVal <= 0.0:
    print('Validation set size must be greater than 0.0')
    exit(1)

# Create a list that pairs together the Segmentation (paths) with their images (paths)

# Separate the list into training and validation and training

# Create a new dataset folder, perhaps called SegDataset, with subfolders 
# ISICTrain, and ISICVal, and ISICTrain

# Copy the images using their paths into their new appropriate folders.
# Under each set folder should be also the folders Images and Segmentations.
# For example, ISICTrain would have ISICTrain/Images and ISICTrain/Segmentations

def findImagePaths(root, subFolder, blackList=None):
    basePath = os.path.join(root, subFolder)

    categoryFolder_files = set(os.listdir(basePath))

    if blackList is not None:
        foundMatches = categoryFolder_files & blackList
        # if len(foundMatches) > 0:
        #     print(foundMatches)
        categoryFolder_files = categoryFolder_files - foundMatches #Remove files that were matched with blacklist

    categoryFolder_paths = [str(os.path.join(basePath, categoryFile)) for categoryFile in categoryFolder_files]
    imagePaths = sorted(categoryFolder_paths)

    return imagePaths

def filterPathsBySegmentation(imagePaths, maskPaths):
    imagePathSet = set(imagePaths)

    finalPathPairs = []

    for maskPath in maskPaths:
        maskFileName = maskPath[maskPath.rfind('/')+1:]
        imageNameGuess = maskFileName[:maskFileName.rfind('_')] + '.jpeg'
        imageDirGuess = imagePaths[0][:imagePaths[0].rfind('/')]
        imagePathGuess = os.path.join(imageDirGuess, imageNameGuess)
        if imagePathGuess in imagePathSet:
            newPair = (imagePathGuess, maskPath)
            finalPathPairs.append(newPair)

    return finalPathPairs

def subsetPaths(pathPairs, percentVal, percentTest):
    assert(percentVal + percentTest <= 1.0)

    totalNum = len(pathPairs)
    numVal = int(float(totalNum) * percentVal)
    numTest = int(float(totalNum)* percentTest)
    numTrain = totalNum - numVal - numTest

    indexList = list(range(len(pathPairs)))
    indexList = np.array(indexList)
    np.random.shuffle(indexList)

    trainPairs = []
    for index in indexList[0:numTrain]:
        trainPairs.append(pathPairs[index])

    valPairs = []
    for index in indexList[numTrain:numTrain+numVal]:
        valPairs.append(pathPairs[index])

    testPairs = []
    for index in indexList[numTrain+numVal:numTrain+numVal+numTest]:
        testPairs.append(pathPairs[index])

    return trainPairs, valPairs, testPairs

print('Collecting image paths...')
imagePaths = findImagePaths(opt.root, 'Images')
print('Collecting mask paths...')
maskPaths = findImagePaths(opt.root, 'Segmentations')

print('Using mask paths to find valid image paths and paring them up.')
pathPairs = filterPathsBySegmentation(imagePaths, maskPaths)

print('Separating dataset into Train, Validation, and Test...')
print('\tTotal Valid Dataset Size:', len(pathPairs))
print('\tValidation Proportion:', opt.percentVal)
print('\tTest Proportion:', opt.percentTest)
trainPairs, valPairs, testPairs = subsetPaths(pathPairs, opt.percentVal, opt.percentTest)
print()
print('\tFinal Train Size:', len(trainPairs))
print('\tFinal Val Size:', len(valPairs))
print('\tFinal Test Size:', len(testPairs))

os.makedirs(opt.output, exist_ok=True)

def cleanDataset(root, subFolder):
    subPath = os.path.join(root, subFolder)    
    imagePath = os.path.join(subPath, 'Images')
    segmentPath = os.path.join(subPath, 'Segmentations')

    if os.path.exists(imagePath):
        rmtree(imagePath)
    
    if os.path.exists(segmentPath):
        rmtree(segmentPath)

def makeDataset(root, subFolder, pairs):
    subPath = os.path.join(root, subFolder)
    os.makedirs(subPath, exist_ok=True)
    
    imagePath = os.path.join(subPath, 'Images')
    segmentPath = os.path.join(subPath, 'Segmentations')

    os.makedirs(imagePath, exist_ok=True)
    os.makedirs(segmentPath, exist_ok=True)

    #Write out the pair paths into a file
    with open(os.path.join(subPath, 'dataset.txt'), 'w') as wf:

        for pair in pairs:
            finalImagePath = copy2(pair[0], imagePath)
            finalImagePath = os.path.join('Images', finalImagePath[finalImagePath.rfind('/')+1:])

            finalMaskPath = copy2(pair[1], segmentPath)
            finalMaskPath = os.path.join('Segmentations', finalMaskPath[finalMaskPath.rfind('/')+1:])

            #Read CV2 Image
            g_img = cv2.imread(pair[1], cv2.IMREAD_GRAYSCALE)
            _, img = cv2.threshold(g_img, 1, 255, cv2.THRESH_BINARY)

            # Extract contours from image
            contours, hierarchy = cv2.findContours(img, cv2.RETR_TREE, cv2.CHAIN_APPROX_SIMPLE)
        
            # Run algorithm to find x, y bounds
            boundingBox = ()

            if len(contours) > 0:
                c = contours[0]
                epsilon = 0.01*cv2.arcLength(c,True)
                cp = cv2.approxPolyDP(c, epsilon, True)
                (x,y), radius = cv2.minEnclosingCircle(cp)
                center = (int(x), int(y))
                radius = int(radius)
                x1 = center[0] - radius
                y1 = center[1] - radius
                x2 = center[0] + radius
                y2 = center[1] + radius

                x1 = min(max(x1, 0), img.shape[1])
                x2 = min(max(x2, 0), img.shape[1])
                y1 = min(max(y1, 0), img.shape[0])
                y2 = min(max(y2, 0), img.shape[0])

                boundingBox = (x1, y1, x2, y2)

                # Write pair line into dataset file
                wf.write('{}\t{}\t'.format(finalImagePath, finalMaskPath))

                # Write bounds in [x1, y1, x2, y2] format
                wf.write('{}\t{}\t{}\t{}\n'.format(
                    boundingBox[0], 
                    boundingBox[1],
                    boundingBox[2],
                    boundingBox[3]
                    ))

print('The following directories will be created and overwritten:')

print(opt.output)
print('\t> ISICTrain')
print('\t\t> Images')
print('\t\t> Segmentations')
print('\t> ISICVal')
print('\t\t> Images')
print('\t\t> Segmentations')

if len(testPairs) > 0:
    print('\t> ISICTest')
    print('\t\t> Images')
    print('\t\t> Segmentations')
    

print('\nThis CANNOT BE UNDONE!')

response = input('\nAre you sure? (Y\\n):')

if response.lower() != 'y':
    print('Exiting...')
    exit(0)

print('Cleaning ISICTrain...')
cleanDataset(opt.output, 'ISICTrain')

print('Cleaning ISICVal')
cleanDataset(opt.output, 'ISICVal')

if len(testPairs) > 0:
    print('Cleaning ISICTest')
    cleanDataset(opt.output, 'ISICTest')

print('Constructing ISICTrain...')
makeDataset(opt.output, 'ISICTrain', trainPairs)
print('Constructing ISICVal...')
makeDataset(opt.output, 'ISICVal', valPairs)

if len(testPairs) > 0:
    print('Constructing ISICTest...')
    makeDataset(opt.output, 'ISICTest', testPairs)