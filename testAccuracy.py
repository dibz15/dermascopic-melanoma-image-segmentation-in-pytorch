import torch

import os
from utils import *
import cv2
import numpy as np
from ISICSegmentDataset import ISICSegmentDataset

import argparse
import matplotlib.pyplot as plt

from tqdm import tqdm

parser = argparse.ArgumentParser(description='FasterRCNN with MobileNetV2 on WIDER Face Dataset.')

#PyTorch Arguments
parser.add_argument('--cuda', action='store_true', help='use cuda?')
parser.add_argument('--checkpoint_folder', type=str, help="directory of checkpoint files")
parser.add_argument('--model_file', type=str, help="pre-trained model file")
parser.add_argument('--score_threshold', type=float, default=0.5, help="Threshold for box scoring.")
parser.add_argument('--dataset_root', type=str, default=os.path.join('.', 'dataset'), help='directory path to ISIC segmentation dataset root')

#OpenCV arguments
parser.add_argument('--imageScale', type=float, default=0.5, help="Amount by which to downscale dataset images.")
parser.add_argument('--max_input_size', type=int, default=1024, help="Amount by which to downscale dataset images.")
parser.add_argument('--grayscale', action='store_true', help='Test with images converted to grayscale?')

#Parse Arguments
opt = parser.parse_args()


if not opt.model_file and not opt.checkpoint_folder:
    print('Please specify either --checkpoint_folder or --model_file')
    quit()
elif opt.model_file and opt.checkpoint_folder:
    print('Please specify just one of either --model_file or --checkpoint_folder')
    quit()

device = torch.device('cuda:0') if opt.cuda and torch.cuda.is_available() else torch.device('cpu')

#===========================================

train_dataset = ISICSegmentDataset(root = os.path.join(opt.dataset_root, 'ISICTest'), 
                                    transforms=getTestTransforms(),
                                    imageScale=opt.imageScale,
                                    blackListFile=None,
                                    minSize=32,
                                    maxSize=opt.max_input_size,
                                    grayscale=opt.grayscale)

#Load and prepare model ====================

if opt.checkpoint_folder:
    model_files = list(sorted(os.listdir(opt.checkpoint_folder)))
else:
    model_files = [opt.model_file]

scores = []

for model_file in model_files:
    if opt.checkpoint_folder:
        model_file_path = os.path.join(opt.checkpoint_folder, model_file)
    else:
        model_file_path = model_file

    print(model_file_path)
    model = torch.load(model_file_path, map_location=device)

    if type(model) == dict:
        if 'full_model' in model.keys() and model['full_model'] is not None:
            model = model['full_model']
        elif 'model_state' in model.keys() and model['model_state'] is not None:
            if 'backbone_state' in model.keys():
                model = getModifiedMaskRCNN(backbone_state=model['backbone_state'], model_state=model['model_state'])
            else:
                model = getModifiedMaskRCNN(model_state=model['model_state'])

    model = model.to(device)
    model.eval()
    #===========================================

    iou_scores = []
    f1_scores = []
    tp = 0
    fp = 0
    fn = 0
    wrong_paths = []
    for image, target in tqdm(train_dataset):
        if image is None or target is None:
            break
        cleanedMaskGT = target['masks'].detach().cpu().numpy()

        modelInput = [image.to(device)]
        pred = model(modelInput)
        sortedDict = sortPredictionsByClass(pred, 1)

        boxes = sortedDict[1]
        boxPicks = nonMaximumSuppression_pick(boxes, confThresh=opt.score_threshold, iouThresh=0.0)
        boxes = boxes[boxPicks]

        if (len(boxes) > 0):
            cleanedMasks = cleanupMasks(pred)
            maskPicks = cleanedMasks[0][boxPicks]

            bestBoxIdx = np.argmax(boxes[:, 4], axis=0)
            mask = maskPicks[bestBoxIdx]

            mask255 = np.array(mask * 255, dtype=np.uint8)
            mask255[np.where(mask255 >= 127)] = 255
            mask255[np.where(mask255 < 127)] = 0

            maskRGB = cv2.cvtColor(mask255, cv2.COLOR_GRAY2RGB)
            maskRGB_gt = cv2.cvtColor(cleanedMaskGT[0] * 255, cv2.COLOR_GRAY2RGB)

            overlappedImg = cv2.bitwise_and(maskRGB, maskRGB_gt)
            unionedImg = cv2.bitwise_or(maskRGB, maskRGB_gt)
            
            maskGT = np.array(cleanedMaskGT[0])
            mask[np.where(mask >= 0.5)] = 1.0
            maskPred = np.array(mask, dtype=np.uint8)

            maskGT_sum = np.sum(maskGT)
            maskPred_sum = np.sum(maskPred)

            intersection = np.sum(np.multiply(maskGT, maskPred))
            union = maskGT_sum + maskPred_sum - intersection

            iou = (intersection + 1) / (union + 1)
            f1 = 2 * intersection / (maskGT_sum + maskPred_sum)

            iou_scores.append(iou)
            f1_scores.append(f1)

            proportionOfGTCovered = maskGT_sum / (intersection + 1)

            if (f1 > 0.5):
                tp += 1
            else:
                fp += 1
                fn += 1
                wrong_paths.append(target['imagePath'])

            # cv2.imshow('GT Mask', maskRGB_gt)
            # cv2.imshow('Masked Image', maskRGB)
            # cv2.imshow('Overlapped', overlappedImg)
            # cv2.imshow('Union', unionedImg)
            # cv2.waitKey()
        else:
            fn += 1
            wrong_paths.append(target['imagePath'])

    # iou_scores = np.array(iou_scores)
    avg_iou = np.mean(iou_scores)
    # f1_scores = np.array(f1_scores)
    avg_f1 = np.mean(f1_scores)
    precision = tp / (tp + fp)
    recall = tp / (tp + fn)
    accuracy = (tp + 0) / (tp + 0 + fp + fn)

    print('\tAvg IoU:', avg_iou)
    print('\tAvg F1:', avg_f1)
    print('\tRecall:', recall)
    print('\tPrecision:', precision)
    print('\tAccuracy:', accuracy)
    print()

    # if opt.model_file:
    #     conf_matrix = createConfusionMatrix(tp, 0, fp, fn)
    #     visualizeConfusionMatrix(conf_matrix, [['True', 'False']] * 2)
    #     print(wrong_paths)
                
    scores.append(avg_f1)

if opt.checkpoint_folder:
    plt.plot(range(len(scores)), scores)
    plt.title(opt.checkpoint_folder)
    plt.show()